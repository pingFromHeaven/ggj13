package  
{
	public class Assets 
	{
		[Embed(source = "imgs/background.png")] public static var imgBackground: Class;
		[Embed(source = "imgs/slideg.png")] public static var imgPlayerCrouch: Class;
		[Embed(source = "imgs/heart.png")] public static var imgHeart: Class;
		[Embed(source = "imgs/cam.png")] public static var imgTile: Class;
		[Embed(source = "imgs/polis.png")] public static var imgSpike: Class;
		[Embed(source = "imgs/doubleSpike.png")] public static var imgDoubleSpike: Class;
		[Embed(source = "imgs/lol.png")] public static var imgBox: Class;
		[Embed(source = "imgs/wc1g.png")] public static var imgTest: Class;
		[Embed(source = "imgs/wc_25frame.png")] public static var imgPlayer: Class;
		[Embed(source = "imgs/bg.png")] public static var imgBg: Class;
		[Embed(source = "imgs/jump.png")] public static var imgJump: Class;
		[Embed(source = "imgs/durma1g.png")] public static var imgDust: Class;
		[Embed(source = "imgs/adrenalineFull.png")] public static var imgAdrenalineFull: Class;
		[Embed(source = "imgs/adrenalineEmpty.png")] public static var imgAdrenalineEmpty: Class;
		[Embed(source = "imgs/Enemy.png")] public static var imgEnemy:Class;
		[Embed(source = "imgs/Health.png")] public static var imgHealthCollectable:Class;
		[Embed(source = "imgs/Adrenaline.png")] public static var imgAdrenalineCollectable:Class;
		[Embed(source = "imgs/music.mp3")] public static var soundTest:Class;
		[Embed(source = "imgs/L1walk.mp3")] public static var soundl1Walk:Class;
		[Embed(source = "imgs/L1Jump1.mp3")] public static var soundl1Jump:Class;
		
		
		public static const TILE_WIDTH: int = 40;
		public static const PLAYER_X: int = 150;
		public static const GROUND_HEIGHT: int = 350;
		public static const GRAVITY: int = 500;
		public static const CHUNK_SIZE: int = 40;
		public static var CHUNKS: Array;
		public static const CHUNKS_SIZE: int = 16;
		public static const FIRST: String = "#########################################";
		public static const ACCELERATION:Number = 0.1;
		public static const DECELERATION:Number = 0.1;
		public static const SPEED_LOWEST_LIMIT:Number = 0.1;
		public static const ADRENALINE_BASE_INCREASE_RATE:Number = 0.05;
		public static const POINT_BASE_INCREASE_RATE:Number = 0.1;
		public static const DOUBLE_JUMP_COST:int = 20;
		public static const IMMUNITY_COST:int = 95;
		public static const KNOCK_COST:int = 20;
		
		
		
		public static function generateChunk(difficulty: int): String
		{
			var str: String = new String("###");
			
			for (var i: int = 3; i < CHUNK_SIZE-3; i++) 
			{
				switch(difficulty)
				{
					case 0:
						var rnd: Number = Math.random();
						if (rnd < 0.85) str += "#";
						else str += "B";
						break;
					case 1:
						rnd = Math.random();
						if (rnd < 0.85) str += "#";
						else if (rnd < 0.9) str += "b";
						else str += "B";
						str += "##";
						i += 2;
						break;
					case 2:
						rnd = Math.random();
						if (rnd < 0.80) str += "#";
						else if (rnd < 0.85) str += "b";
						else if (rnd < 0.95) str += "B";
						else str += "S";
						str += "###"
						i += 3;
						break;
						
					case 3:
						rnd = Math.random();
						if (rnd < 0.75) str += "#";
						else if (rnd < 0.80) str += "S";
						else if (rnd < 0.85) str += "b";
						else if (rnd < 0.95) str += "B";
						else str += "S";
						str += "####"
						i += 4;
						break;
						
					case 4:
						rnd = Math.random();
						if (rnd < 0.75) str += "#";
						else if (rnd < 0.80) str += "X";
						else if (rnd < 0.85) str += "b";
						else if (rnd < 0.95) str += "B";
						else str += "S";
						str += "####"
						i += 4;
						break;
						
					case 5:
						rnd = Math.random();
						if (rnd < 0.70) str += "#";
						else if (rnd < 0.75) str += "X";
						else if (rnd < 0.80) str += "S";
						else if (rnd < 0.85) str += "b";
						else if (rnd < 0.95) str += "B";
						else str += "S";
						str += "#####"
						i += 5;
						break;
						
					case 6:
						rnd = Math.random();
						if (rnd < 0.70) str += "#";
						else if (rnd < 0.75) str += "X";
						else if (rnd < 0.80) str += "S";
						else if (rnd < 0.85) str += "b";
						else if (rnd < 0.95) str += "B";
						else if (rnd < 0.97) str += "P";
						else str += "S";
						str += "#####"
						i += 5;
						break;
						
					case 7:
						rnd = Math.random();
						if (rnd < 0.60) str += "#";
						/*
						else if (rnd < 0.65) str += "X";
						else if (rnd < 0.70) str += "S";
						else if (rnd < 0.75) str += "b";
						else if (rnd < 0.85) str += "B";
						*/
						else if (rnd < 0.80) str += "P";
						else if (rnd < 0.98) str += "Y";
						else str += "S";
						str += "#####"
						i += 5;
						break;						
				}
			}
			
			str += "###";
			
			return str;
		}
		
		public static function generateChunkArray(): void
		{
			CHUNKS = new Array();
			for (var i: int = 0; i < CHUNKS_SIZE; i++)
			{
				var difficulty: int = Math.random() * 8;
				CHUNKS.push(generateChunk(difficulty));
			}
		}
		
		public function Assets() 
		{
			
		}
		
	}

}