package  
{
	import flash.accessibility.Accessibility;
	import org.flixel.*;
	import flash.utils.getTimer;
	
	public class PlayState extends FlxState 
	{
		
		private var player: Player;
		private var map: Map;
		private var pulse:Array;
		public var pulseIndex:int;
		public var lastTimer:int;
		public var curHeartRate:int;
		public var adrenalineRect:FlxSprite = new FlxSprite(50, 50, Assets.imgAdrenalineFull);
		public var adrenalineRectBack:FlxSprite = new FlxSprite(50, 50, Assets.imgAdrenalineEmpty);
		public var adrenalineText:FlxText;
		public var pointsText:FlxText;
		public var points:Number = 0;
		
		public var deadText: FlxText = new FlxText(600, 100, 200, "");
		public var deadTimer: FlxTimer = new FlxTimer();
		
		public var heart1: FlxSprite = new FlxSprite(50, 10,Assets.imgHeart);
		public var heart2: FlxSprite = new FlxSprite(80, 10,Assets.imgHeart);
		public var heart3: FlxSprite = new FlxSprite(110, 10,Assets.imgHeart);
		public var heart4: FlxSprite = new FlxSprite(140, 10,Assets.imgHeart);
		public var livesRemaining: int = 3;
		
		public var mapOldState:int = 0;
		
		public var sound: FlxExtendedSound = new FlxExtendedSound();
		public var lol: FlxSound = new FlxSound();
		
		public function PlayState() 
		{
			
		}
		override public function create():void
		{
			super.create();
			
			var bg: FlxSprite = new FlxSprite(0, 0, Assets.imgBackground);
			add(bg);
			
			FlxG.bgColor = 0xffffffff;
			Assets.generateChunkArray();
			initMap();
			initPlayer();
			pulse = new Array(1000, 1000, 1000, 1000, 1000);
			pulseIndex = 0;
			lastTimer = getTimer();

			adrenalineRect.width = 15;
			adrenalineRect.height = 100;
			adrenalineRectBack.width = 15;
			adrenalineRectBack.height = 15;
			add(adrenalineRectBack);	
			add(adrenalineRect);
			
			adrenalineText = new FlxText(50, 50, 100);
			adrenalineText.color = 0;
			adrenalineText.size = 15;
			add(adrenalineText);
			
			pointsText = new FlxText(600, 50, 200);
			pointsText.color = 0;
			pointsText.size = 25;
			add(pointsText);
			
			deadText.setFormat(null, 24, 0, "center");
			add(deadText);
			
			add(heart1);
			add(heart2);
			add(heart3);
			add(heart4);
			heart1.visible = true;
			heart2.visible = true;
			heart3.visible = true;
			heart4.visible = false;
			
			
			sound.loadEmbedded(Assets.soundl1Walk, true, false);
			sound.volume = 1;
			sound.playbackSpeed = 1;
			//sound.play();
			
		}
		
		private function initMap(): void
		{
			map = new Map();
			add(map);
		}
		
		private function initPlayer(): void
		{
			player = new Player(Assets.PLAYER_X, 200);
			add(player.immunityText);
			add(player);
		}
		
		public function drawHearts(lives: int): void
		{
			switch(lives)
			{
				case 1:
					heart1.visible = true;
					heart2.visible = false;
					heart3.visible = false;
					heart4.visible = false;
					break;
				case 2:
					heart1.visible = true;
					heart2.visible = true;
					heart3.visible = false;
					heart4.visible = false;
					break;
				case 3:
					heart1.visible = true;
					heart2.visible = true;
					heart3.visible = true;
					heart4.visible = false;
					break;
				case 4:
					heart1.visible = true;
					heart2.visible = true;
					heart3.visible = true;
					heart4.visible = true;					
			}
		}
		
		public function killPlayer(t: FlxTimer):void
		{
			if (!(--livesRemaining)) FlxG.switchState(new GameOver(points));
			drawHearts(livesRemaining);
			map.clear();
			map.init();
			player.alive = true;
			player.y = Assets.GROUND_HEIGHT - 100;
			player.adrenaline = 0;
			player.isKnockOn = false;
			player.isImmunityOn = false;
			deadText.text = "";
		}
		
		private function getSound():void
		{
			if (!player.alive) return;
			//Placeholder, keyboard input instead of sound
			if (FlxG.keys.justPressed("L"))
			{
				var curTime:int = getTimer();
				var deltaTime:int = curTime - lastTimer;
				lastTimer = curTime;
				pulse[pulseIndex] = deltaTime;
				pulseIndex++;
				if (pulseIndex == pulse.length) pulseIndex = 0;
				var averageTime:int = 0;
				for (var i:int = 0; i < pulse.length; i++)
				{
					averageTime += pulse[i];
				}
				averageTime /= pulse.length;
				curHeartRate = 60 / (averageTime / 1000);
				var calculatedMovSpeed:int = (curHeartRate * curHeartRate) / (60 * 60 * 2);
				if (map.movSpd < calculatedMovSpeed) map.movSpd += Assets.ACCELERATION;
				else map.movSpd -= Assets.DECELERATION;
			}
			else
			{
				var averageTime:int = 0;
				for (var i:int = 0; i < pulse.length; i++)
				{
					averageTime += pulse[i];
				}
				averageTime += getTimer() - lastTimer;

				averageTime /= pulse.length;
				curHeartRate = 60 / (averageTime / 1000);
				var calculatedMovSpeed:int = (curHeartRate * curHeartRate) / (60 * 60 * 2);
				if (map.movSpd < calculatedMovSpeed)
				{
					if (map.movSpd + Assets.ACCELERATION > calculatedMovSpeed) map.movSpd = calculatedMovSpeed;
					else map.movSpd += Assets.ACCELERATION;
				}
				else map.movSpd -= Assets.DECELERATION;
				if (map.movSpd <= Assets.SPEED_LOWEST_LIMIT) map.movSpd = 0;
			}
			if (map.movSpd == 0)
			{
				map.speedState = -1;
				player.play("dust");
			}
			else if (map.movSpd < 4)
			{
				player.adrenaline -= Assets.ADRENALINE_BASE_INCREASE_RATE;
				map.speedState = 0;
			}
			else if (map.movSpd >= 4 && map.movSpd <= 8)
			{
				player.adrenaline += Assets.ADRENALINE_BASE_INCREASE_RATE;
				map.speedState = 1;
			}
			else if (map.movSpd >= 8 && map.movSpd <= 12)
			{
				player.adrenaline += 2 * Assets.ADRENALINE_BASE_INCREASE_RATE;
				map.speedState = 2;
			}
			else if (map.movSpd > 12)
			{
				player.adrenaline += 3 * Assets.ADRENALINE_BASE_INCREASE_RATE;
				map.speedState = 3;
			}
			if (map.speedState != mapOldState)
			{
				switch(map.speedState)
				{
					case 0:
						player.play("walking");
						break;
					case 1:
						player.play("running");
						break;
					case 2:
						player.play("runningFaster");
						break;
					case 3:
						player.play("sprint");
						break;
				}
				mapOldState = map.speedState;
			}
			if (player.adrenaline < 0) player.adrenaline = 0;
			if (player.adrenaline > 100) player.adrenaline = 100;
			
			//Handle Powerups
			if (FlxG.keys.justPressed("K"))
			{
				player.doubleJump();
			}
			if (FlxG.keys.justPressed("M"))
			{
				player.immunityTriggered();
			}
			if (FlxG.keys.justPressed("T"))
			{
				player.isImmunityOn = !player.isImmunityOn;
				trace(player.isImmunityOn);
			}
			if (FlxG.keys.justPressed("J"))
			{
				player.knockTriggered();
			}
			
			sound.playbackSpeed = (map.movSpd + 0.2) / 5;
		}
		
		private function lolNoob(): void
		{
			deadText.text = "NOOB!";
			player.alive = false;
			FlxG.shake(map.speedState * 0.01 + 0.01, 0.2);
			player.velocity.x = map.speedState*map.speedState*-10;
			player.x = Assets.PLAYER_X;
			map.movSpd = -2;
			deadTimer.start(2, 1, killPlayer);
		}
		
		private function onEnemyCollision(player:Player, enemy:Enemy): void
		{
			if (player.isKnockOn)
			{
				enemy.kill();
				enemy = null;
			}
			else
			{
				lolNoob();
			}
		}
		
		private function onCollectableCollision(player:Player, collectable:Collectable): void
		{
			if (collectable.type == 0)
			{
				if(livesRemaining != 4) livesRemaining ++;
				drawHearts(livesRemaining);
			}
			if (collectable.type == 1)
			{
				player.adrenaline += 20;
				if (player.adrenaline > 100) player.adrenaline = 100;
			}
			collectable.kill();
			collectable = null;
		}
		
		override public function update(): void
		{
			super.update();
			FlxG.collide(player, map.tiles);
			FlxG.collide(player, map.collectables, onCollectableCollision);
			if (map.speedState != 0 && !player.isImmunityOn)
			{
				if (FlxG.collide(player, map.spikes))
				{
					lolNoob();
				}
			}
			
			if (!player.isImmunityOn)
			{
				if (FlxG.collide(player, map.boxes))
				{
					lolNoob();
				}
			}
			
			if (!map.movSpd == 0 && !player.isImmunityOn && player.alive)
			{
				FlxG.collide(player, map.enemies, onEnemyCollision);
			}
			
			if (!player.alive) return;
			
			getSound();
			var temp:int = player.adrenaline;
			adrenalineText.text = temp.toString() + "%";
			adrenalineRect.height = temp;
			if (temp == 0) adrenalineRect.visible = false;
			else
			{
				adrenalineRect.visible = true;
				adrenalineRect.loadGraphic(Assets.imgAdrenalineFull, false, false, temp, 15);
			}
			adrenalineText.x = 35 + temp / 2;
			points = points + Assets.POINT_BASE_INCREASE_RATE * map.movSpd;
			temp = points;
			pointsText.text = "Points: " + temp;
		}
	}

}