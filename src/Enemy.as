package 
{
	import org.flixel.*;

	public class Enemy extends FlxSprite 
	{
		public var speed:Number;
		public function Enemy(x:int, y:int, speed:Number)
		{
			super(x, y, Assets.imgEnemy);
			this.speed = speed;
		}
		
		override public function update():void
		{
			super.update();
			this.x -= speed;
		}
	}
	
}