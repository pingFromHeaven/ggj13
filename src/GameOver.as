package  
{
	import org.flixel.*;
	/**
	 * ...
	 * @author torreng
	 */
	public class GameOver extends FlxState
	{
		
		private var title: FlxText = new FlxText(FlxG.width / 2 - 250, 100, 500, "LOSER.\nHAHA.\nN00BCAKE\nLEARN TO PLAY");
		private var pressSpace: FlxText = new FlxText(FlxG.width / 2 - 100, FlxG.height * 0.87, 200, "space");
		private var pointText: FlxText = new FlxText(FlxG.width / 2 - 100, FlxG.height * 0.6, 200);
		
		public function GameOver(score: int) 
		{
			title.setFormat(null, 32, 0, "center");
			pressSpace.setFormat(null, 9, 0, "center");
			pointText.setFormat(null, 13, 0, "center");
			pointText.text = "SCORE: " + score.toString();
			add(pointText);
			add(title);
			add(pressSpace);
		}
		
		override public function update(): void
		{
			if (FlxG.keys.SPACE) 
			{
				FlxG.switchState(new Menu());
			}
		}
		
	}

}