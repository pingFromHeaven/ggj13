package  
{
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author torreng
	 */
	public class Spike extends FlxSprite 
	{
		
		public function Spike(x: int, y: int) 
		{
			super(x, y, Assets.imgSpike);
			immovable = true;
		}
		
	}

}