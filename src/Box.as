package  
{
	import org.flixel.FlxSprite;
	
	public class Box extends FlxSprite 
	{
		
		public function Box(x: int, y: int) 
		{
			super(x, y, Assets.imgBox);
			immovable = true;
		}
		
	}

}