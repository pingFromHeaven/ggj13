package 
{
	import org.flixel.*;
	
	public class Player extends FlxSprite 
	{
		public var adrenaline:Number = 0;
		public var isImmunityOn:Boolean = false;
		public var isKnockOn:Boolean = false;
		public var immunityTimer:FlxTimer = new FlxTimer();
		public var knockTimer:FlxTimer = new FlxTimer();
		public var immunityText: FlxText = new FlxText(400, 150, 150);
		public var isJumping: Boolean = false;
		public var jumpSound: FlxSound = new FlxSound();
		
		public function Player(X:int, Y:int)
		{
			super(X, Y);
			
			loadGraphic(Assets.imgPlayer,true,false,82,75);
			addAnimation("sprint", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13], 40);
			addAnimation("runningFaster", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13], 30);
			addAnimation("running", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13], 20);
			addAnimation("walking", [14, 15, 16, 17, 18, 19, 20, 21], 10);
			addAnimation("dust", [22], 1, false);
			addAnimation("slide", [23], 1, false);
			addAnimation("jump", [24], 1, false);
			addAnimation("superjump", [25], 1, false);
			addAnimation("idle", [16], 1, false);
			
			this.acceleration.y = Assets.GRAVITY;
			jumpSound.loadEmbedded(Assets.soundl1Jump);
			
		}
		
		override public function update():void
		{
			super.update();
			
			if (isJumping)
			{
				if (isTouching(FLOOR)) 
				{
					isJumping = false;
				}
			}
			
			if(this.alive) this.x = Assets.PLAYER_X;
			handleKeys();
		}

		public function handleKeys(): void
		{
			if (isTouching(FLOOR) && alive)
			{
				if (FlxG.keys.UP) 
				{
					velocity.y = -250;
					isJumping = true;
					jumpSound.play(true);
				}
				if (FlxG.keys.justPressed("DOWN")) 
				{
					play("slide");
					height = height / 2;
					y += height;
				}
			}
			if(FlxG.keys.justReleased("DOWN")) 
			{
				y -= height
				height *= 2;
				play("walking");
			}
		}
		
		public function doubleJump():void
		{
			if (isTouching(FLOOR))
			{
				if (adrenaline >= Assets.DOUBLE_JUMP_COST)
				{
					adrenaline -= Assets.DOUBLE_JUMP_COST;
					velocity.y = -250 * 1.4;
				}
			}
		}
		
		private function onImmunityTimerTick(timer:FlxTimer): void
		{
			isImmunityOn = false;
			immunityText.text = "";
		}
		
		private function onKnockTimerTick(timer:FlxTimer):void
		{
			isKnockOn = false;
		}
		
		public function immunityTriggered():void
		{
			if (isImmunityOn) return;
			if (adrenaline >= Assets.IMMUNITY_COST)
			{
				adrenaline -= Assets.IMMUNITY_COST;
				isImmunityOn = true;
				immunityTimer.start(5, 1, onImmunityTimerTick);
				immunityText.color = 0;
				immunityText.size = 18;
				immunityText.text = "IMMUNITY!";
			}
		}
		
		public function knockTriggered():void
		{
			if (isKnockOn) return;
			if (adrenaline >= Assets.KNOCK_COST)
			{
				adrenaline -= Assets.KNOCK_COST;
				isKnockOn = true;
				knockTimer.start(2, 1, onKnockTimerTick);
			}
		}
	}
	
}