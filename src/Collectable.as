package  
{
	import org.flixel.*;
	public class Collectable extends FlxSprite
	{
		public var type:int;
		public function Collectable(X:int, Y:int, type:int ) 
		{
			this.type = type;
			x = X;
			y = Y;
			if (type == 0) loadGraphic(Assets.imgHealthCollectable);
			if (type == 1) loadGraphic(Assets.imgAdrenalineCollectable);
		}
		
		
	}

}