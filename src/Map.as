package  
{
	import org.flixel.*;
	
	public class Map extends FlxGroup 
	{
		public var movSpd: Number = 1;
		public var ground: FlxGroup = new FlxGroup();
		public var leftChunk:FlxGroup;
		public var currentChunk:FlxGroup;
		public var spikes:FlxGroup;
		public var boxes:FlxGroup;
		public var tiles:FlxGroup;
		public var enemies:FlxGroup;
		public var speedState:int = 0;
		public var collectables:FlxGroup;
		public function Map(): void
		{
			init();
		}
		
		public function init():void
		{
			spikes = new FlxGroup();
			boxes = new FlxGroup();
			tiles = new FlxGroup();
			enemies = new FlxGroup();
			collectables = new FlxGroup();
			movSpd = 0;
			var newChunk:FlxGroup = generateFirst();
			add(newChunk);
			leftChunk = newChunk;
			currentChunk = newChunk;
		}
		
		override public function update(): void
		{
			super.update();
			move();
			if (Math.random() < 0.01)
			{
				var temp:FlxGroup = new FlxGroup();
				var collectable:Collectable = new Collectable(1200, Assets.GROUND_HEIGHT - 60 - Math.random() * 60, Math.random() * 2);
				temp.add(collectable);
				collectables.add(collectable);
				add(temp);
			}
		}
		
		public function move(): void
		{
			for each(var chunk: FlxGroup in members)
			{
				for each(var tile: FlxSprite in chunk.members)
				{
					tile.x -= movSpd;
				}
			}
			if (leftChunk.members[0].x <= 0)
			{
				var newChunk:FlxGroup = generate();
				add(newChunk);
				leftChunk = newChunk;
			}
			if (leftChunk.members[0].x <= Assets.PLAYER_X)
			{
				currentChunk = leftChunk;
			}
		}
		
		public function generate(): FlxGroup
		{
			var chunk: FlxGroup = new FlxGroup();
			var chunkArraySize:int = Assets.CHUNKS.length;
			chunkArraySize--;
			var randomNumber:int = Math.random() * chunkArraySize;
			var rndString: String = Assets.CHUNKS[randomNumber];
			
			for (var i: int = 0; i < rndString.length; i++) 
			{
				var tile: FlxSprite = new FlxSprite(FlxG.width + i * Assets.TILE_WIDTH, Assets.GROUND_HEIGHT, Assets.imgTile);
				tile.immovable = true;
				chunk.add(tile);
				tiles.add(tile);
				if (rndString.charAt(i) == '#') 
				{
					continue;
				}
				else if (rndString.charAt(i) == 'S') 
				{
					var spike: Spike = new Spike(FlxG.width + i * Assets.TILE_WIDTH, Assets.GROUND_HEIGHT - Assets.TILE_WIDTH);
					chunk.add(spike);
					spikes.add(spike);
				}
				else if (rndString.charAt(i) == 'P') 
				{
					var doubleSpike: DoubleSpike = new DoubleSpike(FlxG.width + i * Assets.TILE_WIDTH, Assets.GROUND_HEIGHT - Assets.TILE_WIDTH);
					i++;
					spikes.add(doubleSpike);
					chunk.add(doubleSpike);
				}
				else if (rndString.charAt(i) == 'B') 
				{
					var box: Box = new Box(FlxG.width + i * Assets.TILE_WIDTH, Assets.GROUND_HEIGHT - Assets.TILE_WIDTH);
					boxes.add(box);
					chunk.add(box);
				}
				else if (rndString.charAt(i) == 'b')
				{
					var box: Box = new Box(FlxG.width + i * Assets.TILE_WIDTH, Assets.GROUND_HEIGHT - Assets.TILE_WIDTH * 2);
					boxes.add(box);
					chunk.add(box);
				}
				else if (rndString.charAt(i) == 'X')
				{
					var box: Box = new Box(FlxG.width + i * Assets.TILE_WIDTH, Assets.GROUND_HEIGHT - Assets.TILE_WIDTH);
					boxes.add(box);
					chunk.add(box);
					box = new Box(FlxG.width + i * Assets.TILE_WIDTH, Assets.GROUND_HEIGHT - Assets.TILE_WIDTH * 2);
					boxes.add(box);
					chunk.add(box);
				}
				else if (rndString.charAt(i) == 'Y')
				{
					var enemy:Enemy = new Enemy(FlxG.width + i * Assets.TILE_WIDTH, Assets.GROUND_HEIGHT - Assets.TILE_WIDTH, Math.random() * 3 + 2);
					enemies.add(enemy);
					chunk.add(enemy);
				}
			}
			return chunk;
		}
		
		public function generateFirst(): FlxGroup
		{
			var chunk: FlxGroup = new FlxGroup();
			
			var rndString: String = Assets.FIRST;
			for (var i: int = 0; i < rndString.length; i++) 
			{
				var tile: FlxSprite = new FlxSprite(i * Assets.TILE_WIDTH, Assets.GROUND_HEIGHT, Assets.imgTile);
				tile.immovable = true;
				tiles.add(tile);
				chunk.add(tile);
				if (rndString.charAt(i) == '#') 
				{
					continue;
				}
			}
			return chunk;
		}
		
		
	}

}