package  
{
	import org.flixel.FlxGame;
	import org.flixel.FlxG;
	
	[SWF(width="800", height="500", backgroundColor="#ffffff")]
	
	public class Game extends FlxGame 
	{
		
		public function Game() 
		{
			super(800, 500, Menu);
				
			FlxG.visualDebug = true;
		}
		
	}

}
