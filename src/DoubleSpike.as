package  
{
	import org.flixel.FlxSprite;
	
	/**
	 * ...
	 * @author torreng
	 */
	public class DoubleSpike extends FlxSprite 
	{
		
		public function DoubleSpike(x: int, y: int) 
		{
			super(x, y, Assets.imgDoubleSpike);
			immovable = true;
		}
		
	}

}