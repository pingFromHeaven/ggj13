package  
{
	import org.flixel.*;
	
	public class Menu extends FlxState
	{
		private var title: FlxText = new FlxText(FlxG.width / 2 - 250, 100, 500, "awesome title");
		private var pressSpace: FlxText = new FlxText(FlxG.width / 2 - 100, FlxG.height * 0.87, 200, "Press SPACE");
		
		public function Menu() 
		{
			FlxG.bgColor = 0xffffffff;
			title.setFormat(null, 32, 0, "center");
			pressSpace.setFormat(null, 9, 0, "center");
		
			
			add(title);
			add(pressSpace);
		}
		
		override public function update(): void
		{
			if (FlxG.keys.SPACE) 
			{
				FlxG.switchState(new PlayState());
			}
		}
		
	}

}